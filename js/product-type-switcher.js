function switchType(selectedIndex, className) {

    var elements = new Array();
    elements = document.getElementsByClassName(className);

    for (i in elements) {

        if (i != selectedIndex) {
            elements[i].style.display = "none";
        } else {
            elements[i].style.display = "block";
        }
    }
}
