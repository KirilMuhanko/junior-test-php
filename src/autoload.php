<?php

include_once "psr4_autoloader.php";

$loader = new \Example\Psr4AutoloaderClass;

$loader->register();
$loader->addNamespace('WebApp\Controller', 'src/Controller');
$loader->addNamespace('WebApp\Model', 'src/Model');
$loader->addNamespace('WebApp\Setup', 'src/Setup');
$loader->addNamespace('WebApp\View', 'src/View');
