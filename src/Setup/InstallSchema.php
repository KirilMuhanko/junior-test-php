<?php
namespace WebApp\Setup;

use WebApp\Model\Database;

class InstallSchema
{
    private $db;

    function __construct()
    {
        $this->db = new Database();

        $this->createAttributesTable();
        $this->createProductsTable();
        $this->addAttributes();
        $this->addProducts();
    }
    
    function createAttributesTable()
    {
        $sql = "CREATE TABLE IF NOT EXISTS Attributes (
            attribute_id int NOT NULL,
            attribute_name varchar(255),
            attribute_value varchar(255),
            PRIMARY KEY(attribute_id)
        );";

        $this->db->query($sql);
    }

    function createProductsTable()
    {
        $sql = "CREATE TABLE IF NOT EXISTS Products (
            sku varchar(255) NOT NULL,
            name varchar(255),
            price float,
            attribute_id int NOT NULL,
            value varchar(255),
            PRIMARY KEY(sku),
            FOREIGN KEY(attribute_id) REFERENCES Attributes(attribute_id)
        );";

        $this->db->query($sql);
    }

    function addAttributes()
    {
        $sql = "INSERT INTO Attributes VALUES
            (1, 'Size', 'MB'),
            (2, 'Weight', 'Kg'),
            (3, 'Dimensions', '');";

        $this->db->query($sql);
    }

    function addProducts()
    {
        $sql = "INSERT INTO Products (sku, name, price, attribute_id, value) VALUES
            ('JVC200120', 'Acme DISK', '1', 1, '700'),
            ('JVC200121', 'Acme DISK', '1', 1, '700'),
            ('JVC200122', 'Acme DISK', '1', 1, '700'),
            ('JVC200123', 'Acme DISK', '1', 1, '700'),
            ('GGWP004', 'War and Peace', '20', 2, '2'),
            ('GGWP005', 'War and Peace', '20', 2, '2'),
            ('GGWP006', 'War and Peace', '20', 2, '2'),
            ('GGWP007', 'War and Peace', '20', 2, '2'),
            ('TR120552', 'Chair', '40', 3, '24x45x15'),
            ('TR120553', 'Chair', '40', 3, '24x45x15'),
            ('TR120554', 'Chair', '40', 3, '24x45x15'),
            ('TR120555', 'Chair', '40', 3, '24x45x15');";

        $this->db->query($sql);
    }
}
