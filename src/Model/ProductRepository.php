<?php

namespace WebApp\Model;

use WebApp\Model\Product;
use WebApp\Model\Database;

class ProductRepository
{
    private $db;

    function __construct()
    {
        $this->db = new Database();
    }

    public function fetchProducts(): array
    {
        $dbProducts = $this->getProductsFromDatabase();
        $productArray = [];

        foreach ($dbProducts as $p) {

            $productModel = new Product();
            $productModel->setSku($p['sku']);
            $productModel->setName($p['name']);
            $productModel->setPrice($p['price']);
            $productModel->setValue($p['value']);
            $productModel->setAttributeValue($p['attribute_value']);
            $productModel->setAttributeName($p['attribute_name']);

            $productArray[] = $productModel;
        }

        return $productArray;
    }

    public function addProduct($sku, $name, $price, $attribute_id, $value)
    {
        $sql = 
        "INSERT INTO products (sku, name, price, attribute_id, value) 
        VALUES ('$sku', '$name', $price, $attribute_id, '$value');";

        $this->db->query($sql);
    }

    public function deleteProduct($sku)
    {
        $sql = "DELETE FROM products WHERE sku='$sku';";
        $this->db->query($sql);
    }

    public function deleteProductArray($skuArray)
    {
        foreach ($skuArray as $sku)
        {
            $this->deleteProduct($sku);
        }
    }

    private function getProductsFromDatabase(): array
    {
        $sql =
            "SELECT * FROM products
            INNER JOIN attributes
            ON products.attribute_id=attributes.attribute_id;";

        $arr = $this->db->queryFetchAll($sql);
        return $arr;
    }
}
